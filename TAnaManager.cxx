#include "TAnaManager.hxx"
#include "TV1725Waveform.h"
#include "hiredis.h"

TAnaManager::TAnaManager(){

  wfHistos = new TV1725Waveform();

  redisContext *c;
  redisReply *reply;

  const char *hostname = "127.0.0.1";
  int port = 6379;

  struct timeval timeout = { 1, 500000 }; // 1.5 seconds
  c = redisConnectWithTimeout(hostname, port, timeout);

  if (c == NULL || c->err) {
      if (c) {
          printf("Connection error: %s\n", c->errstr);
          redisFree(c);
      } else {
          printf("Connection error: can't allocate redis context\n");
      }
      exit(-1);
  }

  // delete all histograms

  for (int j=0; j<25; j++) {               // assuming max number of V1725 boards is 25
    for(unsigned int i=0; i<16; i++) {     // reset all channel plots

      char tname[100];
      sprintf(tname, "V1725_B%i_C%i", i, j);
      TH1D *tmp = (TH1D*) gDirectory->Get(tname);
      delete tmp;
    }
  }

  wfHistos->clear();

  // discovery V1725 boards

  for (int b=0; b<25; b++) {     // assuming max number of V1725 boards is 25

     int cm = 0;                 // channel mask
     int wl = 0;                 // waveform length

     reply = (redisReply *) redisCommand(c, "HGET board%d channelmask", b);
     if (reply->type != REDIS_REPLY_NIL) {
        cm = atoi(reply->str);
     }
     freeReplyObject(reply);

     reply = (redisReply *) redisCommand(c, "HGET board%d wflength", b);
     if (reply->type != REDIS_REPLY_NIL) {
        wl = atoi(reply->str);
     }
     freeReplyObject(reply);

     boards.Insert(b, cm, wl);
  }

  std::cout << "Board discovery completed !" << std::endl;

  wfHistos->SetBoards(boards);

  for(int j=0; j<boards.Size(); j++) {

     TV1725Board b = boards.Get(j);
     for(unsigned int i=0; i<b.channelList.size(); i++) {

        char name[100];
        char title[100];
        std::vector<std::string> timescale = {"ns", "us", "ms"};

        sprintf(name, "V1725_B%i_C%i", b.id, b.channelList[i]);
        sprintf(title, "V1725 Waveform for module=%i, channel=%i", b.id, b.channelList[i]);

        double s = b.nsamples * 4;  // 4 ns
        int step = 0;
        while(s > 1000) {
           s = s / 1000;
           step++;
        }

        TH1D *tmp = new TH1D(name, title, b.nsamples, 0, s);

        tmp->SetXTitle(timescale[step].c_str());
        tmp->SetYTitle("ADC value");

        wfHistos->push_back(tmp);
     }
  }

  AddHistogram(wfHistos);
  gettimeofday(&LastUpdateTime, NULL);
};


void TAnaManager::AddHistogram(THistogramArrayBase* histo) {

  histo->DisableAutoUpdate();
  fHistos.push_back(histo);
}


int TAnaManager::ProcessMidasEvent(TDataContainer& dataContainer){

  // Fill all the  histograms
  for (unsigned int i = 0; i < fHistos.size(); i++) {
    // Some histograms are very time-consuming to draw, so we can
    // delay their update until the canvas is actually drawn.
    if (!fHistos[i]->IsUpdateWhenPlotted()) {
      fHistos[i]->UpdateHistograms(dataContainer);
    }
  }
  
  // Only update the transient histograms (like waveforms or event displays) every second.
  // Otherwise hammers CPU for no reason.
  struct timeval nowTime;
  gettimeofday(&nowTime, NULL);
  double dtime = nowTime.tv_sec - LastUpdateTime.tv_sec + (nowTime.tv_usec - LastUpdateTime.tv_usec)/1000000.0;
  if(dtime > 1.0){
    UpdateTransientPlots(dataContainer);
    LastUpdateTime = nowTime;
  }

  return 1;
}


// Little trick; we only fill the transient histograms here (not cumulative), since we don't want
// to fill histograms for events that we are not displaying.
// It is pretty slow to fill histograms for each event.
void TAnaManager::UpdateTransientPlots(TDataContainer& dataContainer){

  std::vector<THistogramArrayBase*> histos = GetHistograms();
  
  for (unsigned int i = 0; i < histos.size(); i++) {
    if (histos[i]->IsUpdateWhenPlotted()) {
      histos[i]->UpdateHistograms(dataContainer);
    }
  }
}
