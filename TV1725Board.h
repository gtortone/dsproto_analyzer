#ifndef _TV1725_BOARD_
#define _TV1725_BOARD_

#include <vector>

class TV1725Board {
public:
   TV1725Board() { };

   ~TV1725Board() { };

   int id;
   std::vector<int> channelList;
   int nsamples;
};

class TV1725BoardArray {
public:
   TV1725BoardArray() { };

   ~TV1725BoardArray() { };

   void Insert(int id, int channelMask, int nsamples);

   int Size() { return boards.size(); };

   TV1725Board Get(unsigned int index) {
      return boards[index];
   };

private:
   std::vector<TV1725Board> boards;
};

#endif
