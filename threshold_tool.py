#!/usr/bin/python3

"""
This script helps users to adjust the V1725 self-trigger thresholds in proto-0.

You can either calculate the baselines and adjust the threshold to be some number of ADC below 
that, or just shift the existing thresholds by a set amount. If calculating baselines, you must
be taking data (so we can look at the waveforms).
"""
import midas
import midas.client
import proto_data_format
import argparse
import baseline_calc
import root_setup
import ROOT

def main():
    parser = argparse.ArgumentParser(description="Tool to calculate baselines and tweak self-trigger thresholds")
    parser.add_argument("--fixed_threshold", type=int, help="Set all the thresholds to a specific value. Only makes sense if you've configured DACs so that all boards have the same baseline.")
    parser.add_argument("--shift_offset", type=int, help="Shift the existing thresholds by this much")
    parser.add_argument("--baseline_offset", type=int, help="Calculate the baseline, and set thresholds to be this number of ADC counts from baseline")
    parser.add_argument("--num_events_avg", type=int, default=25, help="If using --baseline_offset, the number of events to analyze to compute the baseline (defaults to 25)")
    args = parser.parse_args()
    
    if args.baseline_offset is not None:
        if args.shift_offset is not None:
            raise ValueError("Set either --shift_offset or --baseline_offset, not both")
        if args.baseline_offset > 0:
            raise ValueError("--baseline_offset should be a negative number")
    
    c = midas.client.MidasClient("threshold_tool")
    
    thresholds = None
    
    if args.shift_offset is None and args.fixed_threshold is None:
        # Need to calculate baseline
        baselines = baseline_calc.calculate_from_live_events(c, "SYSTEM", args.num_events_avg)
    
        if args.baseline_offset:
            thresholds = {}
    
            for board_idx, chans in baselines.items():
                thresholds[board_idx] = [16000] * 16
                
                for chan, fit_res in chans.items():
                    (mean, rms) = fit_res
                    thresholds[board_idx][chan] = int(mean + args.baseline_offset)
            
    elif args.shift_offset is not None:
        # Simple shift
        thresholds = {}
        
        for i in range(0):
            curr = c.odb_get("/Equipment/V1725_Data00/Settings/Board%i/SelfTrigger_Threshold" % i)
            thresholds[i] = [int(c + args.shift_offset) for c in curr]
    elif args.fixed_threshold is not None:
        # Fixed value
        thresholds = {}
        
        for i in range(0):
            thresholds[i] = [args.fixed_threshold] * 16
            
    if thresholds is not None:
        for i, thresh in thresholds.items():
            c.odb_set("/Equipment/V1725_Data00/Settings/Board%i/SelfTrigger_Threshold" % i, thresh)
            print("New thresholds for board %s: %s" % (i, thresh))  
        print("RESTART THE RUN TO PICK UP THE NEW VALUES!") 
        
if __name__ == "__main__":
    main()
