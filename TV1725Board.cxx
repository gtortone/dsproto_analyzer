#include "TV1725Board.h"

void TV1725BoardArray::Insert(int id, int channelMask, int nsamples) {

   TV1725Board b;

   b.id = id;
   b.nsamples = nsamples;
   for(int i=0; i<16; i++) {
      if (1<<i & channelMask)
         b.channelList.push_back(i);
   }

   boards.push_back(b);
}
