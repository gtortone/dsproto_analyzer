

class ProtoEvent:
    def __init__(self):
        self.v1725s = {}

class V1725Bank:
    def __init__(self, header_4_dwords):
        if not isinstance(header_4_dwords, list):
            raise TypeError("header_4_dwords must be a list")
        
        if len(header_4_dwords) != 4:
            raise ValueError("header_4_dwords must be 4 elements (not %s)" % len(header_4_dwords))
        
        self.global_header = header_4_dwords
        self.waveforms = []
        
    def is_zle(self):
        return ((self.global_header[1] >> 26) & 0x1)
    
    def get_ch_mask(self):
        chMask1 = self.global_header[1] & 0xFF
        chMask2 = ((self.global_header[2] & 0xFF000000) >> 24)
        return chMask1 + (chMask2 << 8)

    def get_num_active_channels(self):
        mask = self.get_ch_mask()
        nActiveChannels = 0
        
        for i in range(16):
          if mask & (1 << i):
            nActiveChannels += 1
        
        return nActiveChannels
    
    def is_channel_active(self, i):
        mask = self.get_ch_mask()
        return mask & (1 << i)
    
    def get_event_size_dwords(self):
        return self.global_header[0] & 0xffffff
    
    def get_num_channel_dwords(self):
        return int((self.get_event_size_dwords() - 4) / self.get_num_active_channels())



def parse(ev):
    """
    Args:
        * ev (midas.event.Event)
    """
    if not ev:
        return None
    
    if "W200" not in ev.banks:
        return None
    
    retval = ProtoEvent()
    err_msg = None
    
    for bank in ev.banks.values():
        if not bank.name.startswith("W2"):
            continue
        
        if len(bank.data) < 4:
            print("Event %i - bank %s is len %s" % (ev.header.serial_number, bank.name, len(bank.data)))
            continue
        
        board_num = int(bank.name.replace("W2", ""))
        board_data = V1725Bank(list(bank.data[:4]))
        pos = 4
        
        if board_data.is_zle():
            raise ValueError("ZLE isn't supported yet")
        
        for i in range(16):
            if not board_data.is_channel_active(i):
                # Add an empty list to keep overall list simple
                board_data.waveforms.append([])
                continue
        
            wf = []
            
            
            
            for j in range(board_data.get_num_channel_dwords()):
                if pos >= len(bank.data):
                    raise RuntimeError("Reached position %s during channel %i; reading %i dwords per channel (%i active); bank length %i" % (pos, i, board_data.get_num_channel_dwords(), board_data.get_num_active_channels(), len(bank.data)))
                
                wf.append(bank.data[pos] & 0x3FFF)
                wf.append((bank.data[pos] >> 16) & 0x3FFF)
                pos += 1
                
                
                
            board_data.waveforms.append(wf)
        
        retval.v1725s[board_num] = board_data
        
    return retval
        
        
