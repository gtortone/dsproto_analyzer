#include <stdio.h>
#include <iostream>
#include <time.h>

#include "TRootanaEventLoop.hxx"
#include "TAnaManager.hxx"
#include "TV1725RawData.h"
#include "TTree.h"

//#define DEBUG

std::string dstPath = "/storage/dsdata/ROOT/";

unsigned countBits(unsigned int mask) {       
  return (int)log2(mask)+1; 
} 

class Analyzer: public TRootanaEventLoop {

public:

  TAnaManager *anaManager;

  TTree *fTree;
  int timestamp;
  int eventnumber;
  int triggertag;
  int channel;
  int nsamples;
  UShort_t *wf;

  Analyzer() {
    UseBatchMode();
  };

  virtual ~Analyzer() {};

  void Initialize() {
  };
  
  void BeginRun(int transition, int run, int time) {  

      fTree = new TTree("midas_data","MIDAS data");
      fTree->Branch("timestamp",&timestamp,"timestamp/I");
      fTree->Branch("eventnumber",&eventnumber,"eventnumber/I");
      fTree->Branch("triggertag",&triggertag,"triggertag/I");
      fTree->Branch("channel",&channel,"channel/I");
  };   

  void EndRun(int transition, int run, int time) {
    std::cout << std::endl; 
  }
  
  bool ProcessMidasEvent(TDataContainer& dataContainer) {

    static bool init = false;
    static int nevents = 0;

    if(GetCurrentRunNumber() == 0) {
       std::cout << "skip conversion due run number not available..." << std::endl;
       exit(0);
    }

    timestamp = dataContainer.GetMidasEvent().GetTimeStamp();
 
    TV1725RawData *v1725 = dataContainer.GetEventData<TV1725RawData>("W200");

    if(v1725) {

       nevents++;
#ifdef DEBUG
       std::cout << "Run number: " << GetCurrentRunNumber() << std::endl;
       std::cout << "Channel Mask: " << v1725->GetChannelMask() << std::endl;
       std::cout << "Channels number: " << countBits(v1725->GetChannelMask()) << std::endl;
       std::cout << "Event Counter: " << v1725->GetEventCounter() << std::endl;
       std::cout << "Trigger Tag: " << v1725->GetTriggerTag() << std::endl;
#endif

       int numberChannelPerModule = countBits(v1725->GetChannelMask());
       for(int ch=0; ch<numberChannelPerModule; ch++) {

         TV1725RawChannel *chdata = new TV1725RawChannel(v1725->GetChannelData(ch));

         eventnumber = v1725->GetEventCounter();
         triggertag = v1725->GetTriggerTag();
         nsamples = chdata->GetNSamples();

         if (!init) {
            init = true;
#ifdef DEBUG
            std::cout << "Initialized waveform with nsamples = " << nsamples << std::endl;
#endif
            fTree->Branch("nsamples",&nsamples,"nsamples/I");
            wf = (UShort_t *) malloc(sizeof(UShort_t) * nsamples);
            fTree->Branch("waveform", wf, "wf[nsamples]/s");
         }

         channel = ch;
#ifdef DEBUG
         std::cout << "Number of samples: " << nsamples << std::endl;
#endif
         for(int s=0; s<chdata->GetNSamples(); s++) {
            //std::cout << "\t sample[" << s << "]: " << chdata->GetADCSample(s);
            wf[s] = chdata->GetADCSample(s);
         }
     
         fTree->Fill();
       }
       
    }

    return true;
  };

  std::string SetFullOutputFileName(int run, std::string midasFilename) {

    char buff[128];
    Int_t in_num = 0, part = 0;
    Int_t num[2] = { 0, 0 }; // run and subrun values
    // get run/subrun numbers from file name
    for (int i=0; ; ++i) {
      char ch = midasFilename[i];
        if (!ch) break;
        if (ch == '/') {
          // skip numbers in the directory name
          num[0] = num[1] = in_num = part = 0;
        } else if (ch >= '0' && ch <= '9' && part < 2) {
          num[part] = num[part] * 10 + (ch - '0');
          in_num = 1;
        } else if (in_num) {
          in_num = 0;
          ++part;
        }
    }
    if (part == 2) {
      if (run != num[0]) {
        std::cerr << "File name run number (" << num[0]
                  << ") disagrees with MIDAS run (" << run << ")" << std::endl;
        exit(1);
      }
      sprintf(buff,"%srun_%.6d_%.4d.root", dstPath.c_str(), run, num[1]);
      printf("Using filename %s\n",buff);
    } else {
      sprintf(buff,"%srun_%.6d.root", dstPath.c_str(), run);
    }

    std::cout << "ROOT filename: " << std::string(buff) << std::endl;
    return std::string(buff);
  };
  
}; 

int main(int argc, char *argv[]) {
  Analyzer::CreateSingleton<Analyzer>();
  return Analyzer::Get().ExecuteLoop(argc, argv);
}

