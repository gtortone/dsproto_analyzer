#include "TV1725Waveform.h"
#include "TV1725Board.h"

#include "TV1725RawData.h"
#include "TDirectory.h"

#include <iostream> 

using namespace std;

/// Reset the histograms for this canvas
TV1725Waveform::TV1725Waveform() {

  SetTabName("V1725 Waveforms");
  SetSubTabName("Unfiltered");
  SetUpdateOnlyWhenPlotted(true);

  SetNanosecsPerSample(4);    // ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
  CreateHistograms();
}

void TV1725Waveform::CreateHistograms(bool force) {
}

void TV1725Waveform::UpdateHistograms(TDataContainer& dataContainer) {

  for (int j=0; j<boards.Size(); j++) {  // loop over boards 

    TV1725Board b = boards.Get(j);

    char name[100];
    sprintf(name, "W2%02i", b.id);
    TV1725RawData *v1725 = dataContainer.GetEventData<TV1725RawData>(name);

    if (v1725) {

      for (unsigned int i=0; i<b.channelList.size(); i++) { // loop over channels
        int index = i + j * b.channelList.size();

        TV1725RawChannel channelData = v1725->GetChannelData(b.channelList[i]);

        for (int j = 0; j < channelData.GetNSamples(); j++) {
          double adc = channelData.GetADCSample(j);
          GetHistogram(index)->SetBinContent(j + 1, adc);

        }
      }
    }
  }
}

void TV1725Waveform::Reset() {

  for (int j=0; j<boards.Size(); j++) {        // loop over modules

    TV1725Board b = boards.Get(j);
    for (unsigned int i=0; i<b.channelList.size(); i++) { // loop over channels
      int index = i + j * b.channelList.size();

      // Reset the histogram...
      for (int ib=0; ib<GetHistogram(index)->GetNbinsX(); ib++) {
        GetHistogram(index)->SetBinContent(ib, 0);
      }

      GetHistogram(index)->Reset();
    }
  }
}
