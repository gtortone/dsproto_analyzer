#!/usr/bin/python3

"""
This script allows a user to adjust V1725 DAC values so that all the channels
have the same baseline.

A run must be in progress so that we can analyze waveforms.

The run must be restarted after making the change to pick up the new values.
"""

import baseline_calc
import argparse
import root_setup
import ROOT
import midas.client

def get_dacs(client):
    dacs = {}
    for board in range(0,4):
        dacs[board] = client.odb_get("/Equipment/V1725_Data00/Settings/Board%i/DC Offset" % board)
        
    return dacs
     
def main():
    parser = argparse.ArgumentParser(description="Tool to equalize baselines of V1725s by adjusting DAC values")
    parser.add_argument("--target", type=int, required=True, help="Adjust the V1725 DAC settings so that all boards have the specified baseline")
    parser.add_argument("--num_events_avg", type=int, default=100, help="The number of events to analyze to compute the baseline (defaults to 100)")
    args = parser.parse_args()
    
    if args.target < 0 or args.target > 16383:
        raise ValueError("Invalid value for --target. Should be 0-16383")
    
    if args.num_events_avg < 1:
        raise ValueError("Invalid value for --num_events_avg. Should be > 0")
    
    client = midas.client.MidasClient("equalize_baselines")
    baselines = baseline_calc.calculate_from_live_events(client, "SYSTEM", args.num_events_avg)
    dacs = get_dacs(client)
    new_dacs = dacs.copy()
    
    # As an example, if a DAC of 5000 gives a baseline of 15150, shifting
    # the DAC to 4000 will give a baseline of 15410.
    
    for board in range(0,4):
        mask = client.odb_get("/Equipment/V1725_Data00/Settings/Board%i/Channel Enable Mask" % board)

        for chan in range(16):
            if not (mask & (1 << chan)):
                continue
            
            delta_bl = args.target - baselines[board][chan][2] # Use max rather than mean
            delta_dac = delta_bl * -3.9
            new_dac = int(dacs[board][chan] + delta_dac)
            
            if new_dac < 0 or new_dac > 65535:
                raise RuntimeError("Unable to set DAC for board %s, channel %s - would need DAC of %s" % (board,chan,new_dac))
            
            new_dacs[board][chan] = new_dac
            
        print("New DACs for board %s - %s" % (board, new_dacs[board]))
    
    for board in range(0,4):
        client.odb_set("/Equipment/V1725_Data00/Settings/Board%i/DC Offset" % board, new_dacs[board])
    
    print("RESTART THE RUN TO PICK UP THE NEW VALUES!") 
                    
    
if __name__ == "__main__":
    main()
    
