#ifndef TV1725RawData_hxx_seen
#define TV1725RawData_hxx_seen

#include <vector>

#include "TGenericData.hxx"


/// Class to store information from a single V1725 ZLE pulse.
class TV1725RawZlePulse {

 public:
  
  /// Constructor
 TV1725RawZlePulse(int firstBin, std::vector<uint32_t> samples):
  fFirstBin(firstBin),fSamples(samples){  };

 TV1725RawZlePulse(){};
  

  /// Get the first bin for this pulse
  int GetFirstBin() const {
    return fFirstBin;    
  }

  /// Get the number of samples.
  int GetNSamples() const {
    return fSamples.size();
  }

  /// Get the first bin for this pulse
  int GetSample(int i) const {
    if(i >= 0 && i < (int)fSamples.size())
      return fSamples[i];
    
    return -1;
  }

 private:
  
  /// The first bin for this ZLE pulse.
  int fFirstBin;

  /// The set of samples for this ZLE pulse.
  std::vector<uint32_t> fSamples;
    

};

/// Class to store information from a single V1725 channel.
/// Class will store either the full ADC waveform (if not compressed)
/// or a vector of TV1725RawZlePulse (if compressed).
class TV1725RawChannel {
  
 public:

  /// constructor
  TV1725RawChannel(int channel, bool iscompressed, int baseline=-1): 
  fChannelNumber(channel),fIsZLECompressed(iscompressed),fBaseline(baseline){
      
  }


  int GetChannelNumber() const {return fChannelNumber;};

  
  bool IsZLECompressed() const {return fIsZLECompressed;};

  int GetBaseline() const {return fBaseline;};  
  
  /// Get the ADC sample for a particular bin (for uncompressed data).
  int GetNSamples() const{return fWaveform.size();};

  /// Get the ADC sample for a particular bin (for uncompressed data).
  int GetADCSample(int i) const{
    if(i > 0 && i < (int)fWaveform.size())
      return fWaveform[i];
    
    // First sample is junk for some reason; use second sample
    if(i==0) return fWaveform[1];

    // otherwise, return error value.
    return -1;
  }

  /// Get the number of ZLE pulses (for compressed data)
  int GetNZlePulses() const {return fZlePulses.size();};
  

  /// Get the ZLE pulse (for compressed data
  TV1725RawZlePulse GetZlePulse(int i) const { 
    if(i >= 0 && i < (int)fZlePulses.size())
      return fZlePulses[i];

    // otherwise, return error value.
    return TV1725RawZlePulse();

  }

  /// Returns true for objects with no ADC samples or ZLE pulses.
  int IsEmpty() const {
    if(fZlePulses.size()==0 && fWaveform.size()==0)
      return true;
    return false;
  }

  /// Add an ADC sample
  /// Warning: this method just adds an ADC sample to the back 
  /// of the vector.  Must add in order and must not add any sample twice.
  void AddADCSample(uint32_t sample){ fWaveform.push_back(sample);};
  
  /// Add an ZLE pulse
  /// Warning: this method just adds a ZLE pulse to the back
  /// of the vector.  Must add in order and must not add any pulse twice.
  void AddZlePulse(TV1725RawZlePulse pulse){ fZlePulses.push_back(pulse);};
  

 private:

  /// Channel number
  int fChannelNumber;

  /// Is ZLE compressed
  bool fIsZLECompressed;

  /// Online baseline (for ZLE data)
  int fBaseline;
  
  std::vector<TV1725RawZlePulse> fZlePulses;
  std::vector<uint32_t> fWaveform;

};


/// Class to store data from CAEN V1725, 250MHz FADC.
///
/// This class encapsulates the data from a single board (in a single MIDAS bank).
/// This decoder is for the default or ZLE version of the firmware.  Not the DPP firmware
class TV1725RawData: public TGenericData {

public:

  /// Constructor
  TV1725RawData(int bklen, int bktype, const char* name, void *pdata);

  /// Get the number of 32-bit words in bank.
  uint32_t GetEventSize() const {return (fGlobalHeader0 & 0xffffff);};

  /// Get the channel mask; ie, the set of channels for which we 
  /// have data for this event.
  uint32_t GetChannelMask() const {return (fGlobalHeader1 & 0xff) + ((fGlobalHeader2 & 0xff000000) >> 16);};

  /// Is the V1725 data ZLE compressed?
  bool IsZLECompressed() const {return ((fGlobalHeader1 >> 26) & 0x1);};

  /// Get event counter
  uint32_t GetEventCounter() const {return ((fGlobalHeader2) & 0xffffff);};

  /// Get trigger tag
  uint32_t GetTriggerTag() const {return ((fGlobalHeader3) & 0xffffffff);};

  /// Get extended time tag
  uint32_t GetExtendedTimeTag() const {return ((fGlobalHeader1 >> 8) & 0xffff);};


  /// Get Number of channels in this bank.
  int GetNChannels() const {return fMeasurements.size();}
  
  /// Get Channel Data
  TV1725RawChannel GetChannelData(int i) {
    if(i >= 0 && i < (int)fMeasurements.size())
      return fMeasurements[i];

    return TV1725RawChannel(0,0);
  }
  

  void Print();


private:

  /// Helper method to handle ZLE compressed data.
  void HandlZLECompressedData();

  /// Helper method to handle uncompressed data.
  void HandlUncompressedData();

  /// The overall global headers
  uint32_t fGlobalHeader0;
  uint32_t fGlobalHeader1;
  uint32_t fGlobalHeader2;
  uint32_t fGlobalHeader3;
  

  /// Vector of V1725 measurements
  std::vector<TV1725RawChannel> fMeasurements;

};

#endif
